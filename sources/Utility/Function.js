import {host,key} from '../Utility/Var'
import AsyncStorage from '@react-native-async-storage/async-storage';
export async function getRequest(url){
    try{
        let response=await fetch(host+url+'api_key='+key,{
            method:"GET",
        })
        let responseJson=await response.json();
        return responseJson
    }catch(error){
        console.log(error)
        return null
    }
}

export const storeData = async (key,value) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      // saving error
    }
}

export const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        // value previously stored
        return value
      }
      return null
    } catch(e) {
      // error reading value
      return null
    }
}

export const signOut=async()=>{
  await AsyncStorage.clear()
}
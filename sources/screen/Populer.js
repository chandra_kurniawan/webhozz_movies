import React from 'react'
import {SafeAreaView,Text} from 'react-native'

export default function App({navigation,route}){
    return(
        <SafeAreaView>
            <Text>Populer {route.params.nama}</Text>
        </SafeAreaView>
    )
}
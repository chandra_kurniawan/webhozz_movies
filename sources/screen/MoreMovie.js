import React,{useState,useEffect} from 'react'
import {SafeAreaView,View,Text,FlatList,Dimensions} from 'react-native'
import {getRequest} from '../Utility/Function'
import Image from 'react-native-fast-image'
import {key,hostImage} from '../Utility/Var'
import Card from '../component/CardMovieVertical'
const {width} = Dimensions.get('window')
const cardWidth=width*0.45
export default function App({navigation,route}){
    const [page,setPage]=useState(1)
    const [data,setData]=useState([])
    async function getMovies(){
        console.log(page)
        let response=await getRequest(route.params.url+"?api_key="+key+"&page="+page)
        if(response!=null){
            setData((val)=>[...val,...response.results])
            //console.log(data)
        }
    }
    useEffect(()=>{
        getMovies()
    },[page])
    return(
        <SafeAreaView style={{flex:1,backgroundColor:"white",}}>
            <FlatList
                data={data}
                numColumns={2}
                contentContainerStyle={{marginHorizontal:20,paddingTop:10}}
                keyExtractor={(item)=>item.id}
                onEndReachedThreshold={0.4}
                onEndReached={()=>{
                    setPage((val)=>val+1)
                }}
                renderItem={({item,index})=>{
                    let thn=item.release_date.split('-')[0]
                    return(
                        <View style={{marginRight:5,width:cardWidth,marginBottom:10}}>
                            <Image source={{uri:hostImage+item.backdrop_path}} resizeMethod="resize" resizeMode="cover" style={{width:cardWidth,height:230}}/>
                            <Text numberOfLines={2} style={{backgroundColor:'rgba(10,135,73,0.5)',color:"white",padding:3,minHeight:40,position:'absolute',bottom:0,width:cardWidth}}>{item.title}</Text>
                            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(10,135,73,0.5)',color:"white"}}>{item.vote_average}</Text>
                            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(183 ,37 ,38,0.5)',color:"white",right:0}}>{thn}</Text>
                        </View>
                    )
                }}
            />
        </SafeAreaView>
    )
}
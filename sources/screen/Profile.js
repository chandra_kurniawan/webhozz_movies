import React,{useEffect,useContext} from 'react'
import {SafeAreaView,Text,TouchableOpacity} from 'react-native'
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
  } from '@react-native-community/google-signin';
import {signOut as logout} from '../Utility/Function'
import { BaseContext } from '../context/BaseContext';
export default function App({navigation}){
    const context=useContext(BaseContext)
    useEffect(()=>{
        GoogleSignin.configure({
            webClientId: '141580159014-ldjl7mc6udcisf50pd3sv0qvd5icr8r3.apps.googleusercontent.com', // client ID of type WEB for your 
        });
    },[])
    return(
        <SafeAreaView style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'white'}}>
            {context.statusLogin?(<TouchableOpacity onPress={()=>signOut()}>
                <Text>Logout</Text>
            </TouchableOpacity>):(
                <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
                    <Text>Kamu harus login</Text>
                </TouchableOpacity>
            )}
        </SafeAreaView>
    )
    async function signOut(){
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          await logout();
          context.setStatusLogin(false,'','')
          //this.setState({ user: null }); // Remember to remove the user from your app's state as well
        } catch (error) {
          console.error(error);
        }
    };
}
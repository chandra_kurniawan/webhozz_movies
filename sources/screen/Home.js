import React,{useEffect,useState} from 'react'
import {SafeAreaView,Text,TouchableOpacity,View,StatusBar,FlatList} from 'react-native'
import Card from '../component/CardMovie'
import {key} from '../Utility/Var'
import {getRequest} from '../Utility/Function'
export default function App({navigation}){
    const [data,setData]=useState([])

    async function getDataTrending(){
        try{
            let response = await getRequest('trending/movie/week?api_key='+key)
            if(response!=null){
                setData(response.results)
            }
        }catch(error){
            console.log(error)
        }
    }
    useEffect(()=>{
        getDataTrending()
    },[]);
    return(
        <SafeAreaView style={{flex:1,backgroundColor:'white'}}>
            <StatusBar backgroundColor="white" barStyle={"dark-content"}/>
            <View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:0.8}}>
                        <Text style={{margin:20,fontWeight:'bold'}}>Trending Movies</Text>
                    </View>
                    <View style={{flex:0.2}}>
                        <Text style={{alignSelf:"flex-end",margin:20,}}>More</Text>
                    </View>
                </View>
                <FlatList
                    contentContainerStyle={{paddingLeft:20}}
                    showsHorizontalScrollIndicator={false}
                    data={data}
                    horizontal
                    keyExtractor={(item)=>item.id}
                    renderItem={({item,index})=><RenderItem item={item}/>}
                />
            </View>
        </SafeAreaView>
    )
    function RenderItem({item}){
        
        return (
            <Card item={item}/>
        )
    }
}




// export default class App extends React.Component{
//     constructor(){

//     }
//     getDataHome(){

//     }
//     componentWillMount(){

//     }
//     render(){
//         return(
//             <SafeAreaView></SafeAreaView>
//         )
//     }
//     componentDidMount(){
//         this.getDataHome()
//     }
// }

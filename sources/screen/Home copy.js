import React,{useEffect,useState} from 'react'
import {SafeAreaView,Text,TouchableOpacity,View} from 'react-native'
import Card,{host,Hello} from '../component/Card'

export default function App({navigation}){
    const [nilai,setNilai]=useState(0)
    function getDataHome(){
        //ambil api home
        
    }
    useEffect(()=>{
        getDataHome()
    },[]);
    return(
        <SafeAreaView style={{alignItems:'center',justifyContent:'center',flex:1}}>
            <Text>{nilai}</Text>

            <TouchableOpacity onPress={()=>{
                setNilai((v)=>v+1)
                //setNilai(2)
            }} style={{borderWidth:1,padding:10,borderRadius:5,marginTop:10}}>
                <Text>Tambah nilai {host}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>{
                navigation.navigate('Popular',{nama:'chandra'});
            }}>
                <Text>Popular</Text>
            </TouchableOpacity>

            <Hello/>
            <Card nama={"Chandra"} nilai={nilai}/>
        </SafeAreaView>
    )
}




// export default class App extends React.Component{
//     constructor(){

//     }
//     getDataHome(){

//     }
//     componentWillMount(){

//     }
//     render(){
//         return(
//             <SafeAreaView></SafeAreaView>
//         )
//     }
//     componentDidMount(){
//         this.getDataHome()
//     }
// }

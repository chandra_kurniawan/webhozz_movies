import React,{useEffect,useState,useContext} from 'react'
import {Text,ScrollView,View,StatusBar,FlatList,TouchableOpacity,Image, Alert} from 'react-native'
import CardMovie from '../component/CardMovie'
import CardTV from '../component/CardTV'
import CardPerson from '../component/CardPerson'
import {key} from '../Utility/Var'
import {getRequest,getData} from '../Utility/Function'
import { BaseContext } from '../context/BaseContext'
import database from '@react-native-firebase/database';
import Ant from 'react-native-vector-icons/AntDesign'
export default function App({navigation}){
    const context=useContext(BaseContext)
    const [movie,setMovie]=useState([])
    const [tv,setTv]=useState([])
    const [person,setPerson]=useState([])
    const [dataFavorite,setDataFavorite]=useState([]);
    const [user,setUser]=useState({
        name:'',
        photo:'',
        email:""
    })

    async function getDataTrending(tipe){
        let response = await getRequest('trending/'+tipe+'/week?')
        if(response!=null){
            if(tipe=='movie'){
                setMovie(response.results)
            }else if(tipe=='tv'){
                setTv(response.results)
            }else if(tipe=='person'){
                setPerson(response.results)
            }
        }
    }
    async function getStorageUser(){
        let user=await getData('user')
        if(user!=null){
            let userx=JSON.parse(user)
            setUser({
                name:userx.name,
                photo:userx.photo,
                email:userx.email
            })
        }
    }
    async function getDataFavorite(){
        if(context.statusLogin){
            database()
                .ref('/favorite/'+user.name)
                .on('value', snapshot => {
                    setDataFavorite((val)=>[...snapshot.val()])
                    //console.log(snapshot.val())
                });
        }
        
    }
    useEffect(()=>{
        getDataTrending('movie')
        getDataTrending('tv')
        getDataTrending('person')
        
    },[]);
    useEffect(()=>{
        getStorageUser()
        getDataFavorite()
    },[context.statusLogin])
    return(
        <ScrollView style={{backgroundColor:'white'}}>
            <StatusBar backgroundColor="white" barStyle={"dark-content"}/>
            {context.statusLogin?(
            <View style={{padding:20,paddingBottom:0,flexDirection:'row',justifyContent:'space-between'}}>
                <Text style={{fontWeight:'bold'}}>{user.name}</Text>
                <Image style={{width:30,height:30,borderRadius:50}} source={{uri:user.photo}} />
            </View>):(
                <View style={{flexDirection:'row',paddingHorizontal:20}}>
                    <View style={{flex:0.8}}>
                        <Text>Nikmati kelebihan aplikasi ini setelah anda melakukan login</Text>
                    </View>
                    <View style={{flex:0.2,alignItems:'flex-end'}}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
                            <Text style={{color:'rgba(10,135,73,0.5)'}}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            <View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:0.8}}>
                        <Text style={{margin:20,fontWeight:'bold'}}>Trending Movies</Text>
                    </View>
                    <TouchableOpacity onPress={()=>{navigation.navigate('MoreMovie',{url:'trending/movie/week',title:'Trending Movie'})}} style={{flex:0.2}}>
                        <Text style={{alignSelf:"flex-end",margin:20,}}>More</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    contentContainerStyle={{paddingLeft:20}}
                    showsHorizontalScrollIndicator={false}
                    data={movie}
                    horizontal
                    keyExtractor={(item)=>item.id}
                    renderItem={({item,index})=>{
                        let cek=dataFavorite.filter((val)=>val.id==item.id)
                        return(
                            <CardMovie item={item}>
                                <TouchableOpacity onPress={()=>setFavorite(item)} style={{position:'absolute',bottom:42,left:0,backgroundColor:'rgba(255,255,255,0.2)',padding:3}}>
                                    <Ant name={cek.length>0?"heart":"hearto"} size={20} color={'red'}/>
                                </TouchableOpacity>
                            </CardMovie>
                    )}}
                />
            </View>
            <View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:0.8}}>
                        <Text style={{margin:20,fontWeight:'bold'}}>Trending TV</Text>
                    </View>
                    <View style={{flex:0.2}}>
                        <Text style={{alignSelf:"flex-end",margin:20,}}>More</Text>
                    </View>
                </View>
                <FlatList
                    contentContainerStyle={{paddingLeft:20}}
                    showsHorizontalScrollIndicator={false}
                    data={tv}
                    horizontal
                    keyExtractor={(item)=>item.id}
                    renderItem={({item,index})=><CardTV item={item}/>}
                />
            </View>
            <View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:0.8}}>
                        <Text style={{margin:20,fontWeight:'bold'}}>Trending Person</Text>
                    </View>
                    <View style={{flex:0.2}}>
                        <Text style={{alignSelf:"flex-end",margin:20,}}>More</Text>
                    </View>
                </View>
                <FlatList
                    contentContainerStyle={{paddingLeft:20}}
                    showsHorizontalScrollIndicator={false}
                    data={person}
                    horizontal
                    keyExtractor={(item)=>item.id}
                    renderItem={({item,index})=><CardPerson item={item}/>}
                />
            </View>
        </ScrollView>
    )
    async function setFavorite(item){
        //console.log(user.email)
        if(context.statusLogin){
            let dt=[];
            database()
                .ref('/favorite/'+user.name)
                .once('value', snapshot => {
                    let bef=snapshot.val();
                    if(bef==null){

                    }else{
                        dt=snapshot.val()
                    }
                    let cek=dataFavorite.filter((val)=>val.id==item.id)
                    if(cek.length==0){
                        dt.push({
                            id: item.id,
                            release_date: item.release_date,
                            title: item.title,
                            vote_average: item.vote_average,
                            poster_path: item.poster_path,
                            backdrop_path: item.backdrop_path,
                        })
                        database()
                            .ref('/favorite/'+user.name)
                            .set(dt)
                            .then(() => console.log('Data set.'));
                    }
                    
                });
        }else{
            Alert.alert('Pemberitahuan','Kamu harus login terlebih dahulu')
        }
        
    }
}

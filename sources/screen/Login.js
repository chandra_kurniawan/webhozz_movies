import React,{useEffect,useContext} from 'react'
import {SafeAreaView,View,Text,TouchableOpacity} from 'react-native'
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';
import {storeData} from '../Utility/Function'
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/database';
import { BaseContext } from '../context/BaseContext';
export default function App({navigation}){
    const context=useContext(BaseContext)
    useEffect(()=>{
        // 
    },[])
    useEffect(()=>{
        GoogleSignin.configure({
            webClientId: '141580159014-ldjl7mc6udcisf50pd3sv0qvd5icr8r3.apps.googleusercontent.com', // client ID of type WEB for your 
        });
    },[])
    return(
        <SafeAreaView style={{flex:1,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>signIn()} style={{backgroundColor:"#E94235",padding:10,borderRadius:5}}>
                <Text style={{color:'white'}}>Login Google</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
    async function signIn(){
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            
            let user=userInfo.user
            storeData('user',JSON.stringify(user));
            context.setStatusLogin(true,user.name,user.photo)
            navigation.goBack()
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
              // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              // play services not available or outdated
            } else {
              // some other error happened
            }
            console.log(error)
        }
    }
}
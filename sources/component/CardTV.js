import React from 'react'
import {View,Text} from 'react-native'
import Image from 'react-native-fast-image'
import {hostImage} from '../Utility/Var'
export default function App({item}){
    let thn=item.first_air_date.split('-')[0]
    return(
        <View style={{marginRight:5,width:100}}>
            <Image source={{uri:hostImage+item.poster_path}} resizeMethod="resize" resizeMode="stretch" style={{width:100,height:150}}/>
            <Text numberOfLines={2} style={{backgroundColor:'rgba(10,135,73,0.5)',color:"white",padding:3,minHeight:40}}>{item.original_name}</Text>
            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(10,135,73,0.5)',color:"white"}}>{item.vote_average}</Text>
            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(183 ,37 ,38,0.5)',color:"white",right:0}}>{thn}</Text>
        </View>
    )
}
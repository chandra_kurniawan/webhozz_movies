import React from 'react'
import {View,Text} from 'react-native'
import Image from 'react-native-fast-image'
import {hostImage} from '../Utility/Var'
export default function App({item}){
    return(
        <View style={{marginRight:15}}>
            <Text style={{marginBottom:5,alignSelf:'flex-end'}}>{item.name}</Text>
            <View>
            <View style={{flexDirection:"row",marginLeft:60}}>
            {item.known_for.map((dt,i)=>{
                return(
                    <Image key={i} style={{width:90,height:120}} source={{uri:hostImage+dt.poster_path}} resizeMode={"stretch"}/>
                )
            })}
            </View>
            <Image source={{uri:hostImage+item.profile_path}} resizeMode={"cover"} style={{width:120,height:120,borderRadius:120,position:'absolute',top:0,borderWidth:1,borderColor:'rgba(10,135,73,0.5)'}}/>
            </View>
        </View>
    )
}
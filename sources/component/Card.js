import React from 'react'
import {View,Text} from 'react-native'

export default function Card({nama,nilai}){
    return(
        <View style={{minHeight:100,minWidth:80,borderWidth:1,borderRadius:5,marginTop:10}}>
            <Text>Ini Card {nama} {nilai}</Text>
        </View>
    )
}

export function Hello(){
    return(
        <Text>Hello</Text>
    )
}

export const host="https://google.com"
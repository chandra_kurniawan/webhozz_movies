import React from 'react'
import {View,Text,Dimensions} from 'react-native'
import Image from 'react-native-fast-image'
import {hostImage} from '../Utility/Var'
const {width} = Dimensions.get('window')
const cardWidth=width*0.45
export default function App({item}){
    let thn=item.release_date.split('-')[0]
    return(
        <View style={{marginRight:5,width:cardWidth,marginBottom:10}}>
            <Image source={{uri:hostImage+item.backdrop_path}} resizeMethod="resize" resizeMode="cover" style={{width:cardWidth,height:230}}/>
            <Text numberOfLines={2} style={{backgroundColor:'rgba(10,135,73,0.5)',color:"white",padding:3,minHeight:40,position:'absolute',bottom:0,width:cardWidth}}>{item.title}</Text>
            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(10,135,73,0.5)',color:"white"}}>{item.vote_average}</Text>
            <Text style={{position:'absolute',padding:5,backgroundColor:'rgba(183 ,37 ,38,0.5)',color:"white",right:0}}>{thn}</Text>
        </View>
    )
}
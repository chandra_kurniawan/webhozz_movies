import React,{createContext} from 'react'

export const BaseContext=createContext()

export default class BaseContextProvider extends React.Component{
    state={
        statusLogin:false,
        name:'',
        photo:''
    }
    setStatusLogin(statusLogin,name,photo){
        this.setState({
            statusLogin:statusLogin,
            name:name,
            photo:photo
        })
    }
    render(){
        return(
            <BaseContext.Provider value={{...this.state,
                setStatusLogin:this.setStatusLogin.bind(this)
            }}>
                {this.props.children}
            </BaseContext.Provider>
        )
    }
}

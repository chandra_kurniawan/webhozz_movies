import React,{useEffect,useState} from 'react'
import {Alert} from 'react-native'
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs'
import Ionicon from 'react-native-vector-icons/Ionicons'
import Ant from 'react-native-vector-icons/AntDesign'
import Fea from 'react-native-vector-icons/Feather'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
const Tab =createMaterialBottomTabNavigator();
import Favorite from '../screen/Favorite'
import Profile from '../screen/Profile'
import Home from '../screen/Home'
import Trending from '../screen/Trending'
import messaging from '@react-native-firebase/messaging';
import database from '@react-native-firebase/database';

import {getData} from '../Utility/Function'
export default function App({}){
    //home, favorite, profile
    
    const [name,setName]=useState('')
    async function getUserName(){
        let user=await getData('user')
        if(user!=null){
            let userx=JSON.parse(user)
            messaging()
                .getToken()
                .then(token => {
                    //console.log(token)
                    database()
                        .ref('/token/'+userx.name)
                        .set({
                            token: token
                        })
                        .then(() => console.log('Data set.'));
            });
            //setName(userx.name)
        }
        //return null
    }
    useEffect(()=>{
        getUserName()
        //console.log(name)
        
    },[])
    useEffect(() => {
        const unsubscribe = messaging().onMessage(async remoteMessage => {
          Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        });
        return unsubscribe;
    }, []);
    useEffect(()=>{
        messaging().setBackgroundMessageHandler(async remoteMessage => {
            console.log('Message handled in the background!', remoteMessage);
        });
        messaging().onNotificationOpenedApp(handleNavigation);
    },[])
    const handleNavigation=async(remoteMessage)=>{
        navigation.navigate('Favorite')
    }
    return(
        <Tab.Navigator activeColor={"black"} inactiveColor={"grey"} barStyle={{
            backgroundColor:"white"
        }}>
            <Tab.Screen name="Trending" options={{
                tabBarIcon:({color})=>{
                    return(
                        <Fea name="trending-up" size={20} color={color}/>
                    )
                }
            }} component={Trending}/>
            <Tab.Screen name="Favorite" options={{
                tabBarIcon:({color})=>{
                    return(
                        <Ant name="hearto" size={20} color={color}/>
                    )
                }
            }} component={Favorite}/>
            <Tab.Screen name="Movies" options={{
                tabBarIcon:({color})=>{
                    return(
                        <MCI name="movie-open" size={20} color={color}/>
                    )
                }
            }} component={Home}/>
            <Tab.Screen name="TV" options={{
                tabBarIcon:({color})=>{
                    return(
                        <Fea name="tv" size={20} color={color}/>
                    )
                }
            }} component={Favorite}/>
            <Tab.Screen name="Profile" options={{
                tabBarIcon:({color})=>{
                    return(
                        <Ionicon name="people-outline" size={20} color={color}/>
                    )
                }
            }} component={Profile}/>
        </Tab.Navigator>
    )
}
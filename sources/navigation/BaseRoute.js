import React,{useEffect,useContext,useState} from 'react'
import {BackHandler,Alert} from 'react-native'
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';
const Stack = createStackNavigator();

import BottomTab from './BottomTab'
import Home from '../screen/Home'
import Popular from '../screen/Populer'
import MoreMovie from '../screen/MoreMovie'
import Login from '../screen/Login'
import {getData} from '../Utility/Function'
import { BaseContext } from '../context/BaseContext';

export default function MyStack() {
    const context=useContext(BaseContext)
    const [initRoute,setInitRoute]=useState('Homes')
    async function requestUserPermission() {
        const authStatus = await messaging().requestPermission();
        const enabled =
          authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
          authStatus === messaging.AuthorizationStatus.PROVISIONAL;
      
        if (enabled) {
          console.log('Authorization status:', authStatus);
        }
    }
    useEffect(() => {
        const backAction = () => {
          Alert.alert("Hold on!", "Are you sure you want to go back?", [
            {
              text: "Cancel",
              onPress: () => null,
              style: "cancel"
            },
            { text: "YES", onPress: () => BackHandler.exitApp() }
          ]);
          return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, []);
    
    useEffect(()=>{
        async function getDataStorage(){
            let user=await getData('user');
            if(user!=null){
                let json=JSON.parse(user);
                context.setStatusLogin(true,json.name,json.photo)
            }
        }
        getDataStorage()
        requestUserPermission();
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    console.log(
                        'Notification caused app to open from quit state:',
                        remoteMessage.notification,
                    );
                    setInitRoute('Login'); // e.g. "Settings"
                    
                }
                //setLoading(false);
        });
    },[])
  return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={initRoute}>
                <Stack.Screen name="Homes" options={{
                    title:"Home",
                    headerTitleAlign:'center',
                    headerShown:false,
                    
                }} component={BottomTab} />
                <Stack.Screen name="Popular" options={{
                    headerTintColor:'red',
                    headerTransparent:true
                }} component={Popular}/>
                <Stack.Screen name="MoreMovie" options={({navigation,route})=>({
                    headerTintColor:'rgba(10,135,73,1)',
                    headerTransparent:false,
                    title:route.params.title
                })} component={MoreMovie}/>
                <Stack.Screen name="Login" options={({navigation,route})=>({
                    headerTintColor:'rgba(10,135,73,1)',
                    headerTransparent:false,
                    //title:route.params.title,
                    headerShown:false
                })} component={Login}/>
            </Stack.Navigator>
        </NavigationContainer>
  );
}
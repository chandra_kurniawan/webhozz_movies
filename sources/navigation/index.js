import React from 'react'
import BaseContext from '../context/BaseContext'
import BaseRoute from './BaseRoute'
export default function App(){
    return(
        <BaseContext>
            <BaseRoute/>
        </BaseContext>
    )
}
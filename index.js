/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import homes from './sources/screen/Home'
import Routes from './sources/navigation/index'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Routes);
